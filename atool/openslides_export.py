#!/usr/bin/env python3

import argparse
import csv
from datetime import date
import akkred.dice
import time

today = date.fromtimestamp(time.time())


parser = argparse.ArgumentParser(description='Import a JVerein CSV into the DB')
parser.add_argument("output", help="CSV to export")
args = parser.parse_args()


cols = [
    "pronoun",
    "is_physical_person",
    "title",
    "first_name",
    "last_name",
    "structure_level",
    "member_number",
    "is_active",
    "default_password",
    "email",
    "username",
    "gender",
    "vote_weight",
    "groups",
    "number",
    "is_present",
    "locked_out",
    "saml_id",
    "comment",
]

from tools import H2SqlClient
client = H2SqlClient()

buchungsarten = client.req("SELECT ID, BEZEICHNUNG FROM BUCHUNGSART")
buchungsarten = dict(buchungsarten)

def get_mitgliedskontoauszug(myId):
    sql_mitgliedIstPS = """SELECT B.ID, B.BETRAG, B.DATUM, B.ZWECK, K.BEZEICHNUNG, B.BUCHUNGSART FROM BUCHUNG as B, MITGLIED as M, MITGLIEDSKONTO AS MK, KONTO AS K WHERE M.ID = %(id)s AND B.MITGLIEDSKONTO = MK.ID AND MK.MITGLIED = M.ID AND B.KONTO = K.ID ORDER BY B.DATUM"""
    result = client.req(sql_mitgliedIstPS % {'id': myId})
    konto_ist = []
    for buchung in result:
        myDict =  {
            'buchungs_id':buchung[0],
            'betrag': buchung[1],
            'buchungs_datum' : buchung[2],
            'zweck': buchung[3],
            'konto_bezeichnung': buchung[4],
            'buchungsart_id': buchung[5],
            }
        if myDict['buchungsart_id'] in buchungsarten:
            myDict['buchungsart'] = buchungsarten[myDict['buchungsart_id']]
        else:
            myDict['buchungsart'] = ""
        konto_ist.append(myDict)
    sql_mitgliedSollPS = """SELECT MK.ZWECK1, MK.BETRAG FROM Mitglied AS M, MITGLIEDSKONTO AS MK WHERE M.ID = %(id)s AND M.ID = MK.Mitglied ORDER BY MK.ID"""
    result = client.req(sql_mitgliedSollPS % {'id': myId})
    konto_soll = []
    for buchung in result:
        myDict = {
            'zweck':buchung[0],
            'betrag': buchung[1],
            }
        konto_soll.append(myDict)
    return {
            'ist': konto_ist,
            'soll': konto_soll,
            }

vorstand = [
    "Lars Andresen",
    "Jonas Martin",
    "Helena Schmidt",
    "Nele Warnecke",
    "Marie Goetz",
    "Chris Fiege",
]
gefundener_vorstand = []
ordentliche_mitglieder = 0
stimmberechtigte_ordentliche_mitglieder = 0
foerdermitglieder = 0

mitglieder = client.req("select M.ID, M.NAME, M.VORNAME, M.ADRESSIERUNGSZUSATZ, M.EMAIL, M.EINTRITT, M.AUSTRITT, B.BEZEICHNUNG from MITGLIED as M, BEITRAGSGRUPPE as B WHERE M.BEITRAGSGRUPPE = B.ID")

with open(args.output, "w") as fh_out:
    writer = csv.DictWriter(fh_out, cols, delimiter=",")
    writer.writeheader()
    for m in mitglieder:
        m = dict(zip(["PK", "Name", "Vorname", "Nick", "Email", "Eintritt", "Austritt", "Beitragsgruppe"], m))
        print(f"Mitglied: {m['Vorname']} {m['Name']} ({m['Nick']}): ", end="")
        if m["Austritt"]:
            if m["Austritt"] < today:
                print("Ausgetreten → Übersprungen")
                continue

        konto = get_mitgliedskontoauszug(m["PK"])
        sum_ist = sum([b["betrag"] for b in konto["ist"] if b["buchungsart"] == "(E) IdB: Mitgliedsbeitrag"])
        sum_soll = sum(b["betrag"] for b in konto["soll"] if "Mitgliedsbeitrag" in b["zweck"])
        print(f" Soll: {sum_soll}, Ist: {sum_ist}, ", end="")

        gruppe = m["Beitragsgruppe"]
        gruppen = []
        if "Fördermitglied" in gruppe:
            gruppen.append("Fördermitgiedys")
            foerdermitglieder += 1
        elif gruppe == "Normalzahlend" or gruppe == "Ermäßigt":
            ordentliche_mitglieder += 1
            if sum_ist >= sum_soll:
                stimmberechtigte_ordentliche_mitglieder += 1
                gruppen.append("Stimmberechtigte Mitgliedys")
                if f"{m['Vorname']} {m['Name']}" not in vorstand:
                    gruppen.append("Stimmberechtigte Mitgliedys (ohne Vorstand)")
                else:
                    gefundener_vorstand.append(f"{m['Vorname']} {m['Name']}")
            else:
                gruppen.append("Nicht Stimmberechtige Mitgliedys")
        else:
            raise ValueError(f"Unbekannte gruppe {gruppe}")
        print(f"Gruppen: {gruppen} ", end="")

        row = {
            "first_name": f"{m['Vorname']} {m['Name']}",
            "last_name": f"({m['Nick']})" if m["Nick"] else "",
            "username": m["Email"],
            "email": m["Email"],
            "groups": ", ".join(gruppen),
        }
        writer.writerow(row)
        print("OK")

print(f"Gefundener Vorstand: {gefundener_vorstand}")
print(f"Ordentliche Mitglieder: {ordentliche_mitglieder}")
print(f"- davon stimmberechtigt: {stimmberechtigte_ordentliche_mitglieder}")
print(f"- davon NICHT stimmberechtigt: {ordentliche_mitglieder - stimmberechtigte_ordentliche_mitglieder}")
print(f"Fördermitglied: {foerdermitglieder}")
