Akkreditierungs-Tool
====================

In diesem Repo findest Du eine Sammlung von Werkzeugen, die den Stratum 0 bei
der Durchführung der Online-Mitgliederversammlungen seit 2021 unterstützt haben.

Vermutlich funktioniert dein Verein nicht wie unserer
(es sei denn Du möchtest eine Mitgliederversammlung für den Stratum 0
vorbereiten...).
Aber vielleicht kannst du Teile unserer Werkzeuge als Steinbruch verwenden,
damit Du weniger $DINGE selber coden musst.

Historie
--------

Unsere Mitgliederversammlungen haben sich über die Jahre entwickelt.
Und genau so ist es auch mit diesem Tool.
Für die unterschiedlichen Jahre gibt es Branches, die den jeweils verwendeten Stand in dem Jahr
enthalten.
Grob gibt es folgende Evolutionsschritte:

* Digitalisiere, wie eine Bundesbehörde: Wir haben exakt den Offline-Workflow in ein digitales Tool
  gezwängt. Inclusive einem Jitsi für eine echte face-2-face Akkreditierung.
  Dazu haben wir Briefwahl mit richtigen Briefen gemacht. Dazu haben wir aktuelle Anschriften von allen
  Mitgliedern eingesammelt.
* Wir haben die face-2-face Akkreditierung fallen gelassen. Jedes stimmberechtigte Mitglied bekommt im Online-Tool
  Stimmrecht.
  Es gab aber weiterhin Briefwahl mit der Adresssammlung.
* Wir haben die Briefwahl fallen gelassen. Dieses Tooling ist auf eine einzelnes Script zusammen geschmolzen, dass
  Benutzerkonten für das Openslides vorbereitet.
  Hierzu greifen wir nun auch direkt in die JVerein Datenbank.

Was es hier zu sehen gibt
-------------------------

In diesem Repo findest Du die folgenden Tools:
(Wobei nicht alle Tools in allen Branches vorhanden sind...)

* Das Akkreditierungs-Tool:
  In dieser Django-basierten Web-Anwendung haben wir unsere Akkreditieung
  durchgeführt.
  Im Wesentlichen ist dieses Tool eine Web-Warteschlange, die die Nutzer
  in einen Jitsi-Raum  mit den Backend-Team bringt.
* ``csv_import.py``: einen Importer, mit dem wir aus einem
  JVerein-Datenbankdump Mitglieder in das Akkreditierungstool importieren
  können.
* ``send_invite_email.py``: Ein Script um Einladungs-Emails zum
  Akkreditierungs-Tool zu verschicken.
* ``openslides_export.py``: Ein Script um Benutzerkonten für OpenSlides zu
  exportieren.
* ``coverlettery.py``: Ein Script um Anschreiben und Wahlscheine für
  die Briefwahl zu erzeugen.

Generell gilt hier: *Code is Documentation!*

Django Web-Anwendung
--------------------

Diese Anwendung setzt stark auf die Kooperation der Mitglieder.
So ist z.B. jedem Mitglied nach der Akkreditierung der *geheime*
Jitsi-Raum bekannt.
Wenn Du erwartest, dass Nicht-Kooperative Entitäten zu deiner MV kommen
solltest Du hier noch mehr Gedanken herein stecken.

Inbetriebnahme
..............

* Python ``venv`` aufsetzen.
* Abhängigkeiten installieren: ``pip install -r REQUIREMENTS.txt``
* SQLite Datenbank aufsetzen: ``./manage.py migrate``
* ``./manage.py createsuperuser``, damit man den Django Admin nutzen kann

Anschließend kann man mit ``./manage.py runserver`` den Devel-Server starten.
Das Frontend ist nun unter ``http://localhost:8000`` zu erreichen.
Das Backend ist unter ``http://localhost:8000/backend`` zu erreichen.

Deployment
..........

Folgendes ist beim Deployment zu beachten:

* Einen ordentlichen Runner verwenden, z.B. ``uwsgi``.
  Der Developemnt-Server ist ungeeignet.
* Die URLs ``/backend`` und ``/admin`` mindestens mit HTTP Basic Authentication
  schützen.
* Auf dem Server liegen später personenbezogene Daten.
  Sorgt dafür, dass die Maschine aktuell und sinnvoll gegen Zugriff gesichter
  ist!
* Richte ``https://`` ein, damit die Desktop-Notifications und das eingebettete
  Jitsi funktionieren.

Container-Deployment (aka hipster me like it's 2024)
....................................................

(Dieser Absatz ist WIP.)

* ``podman build .``
* ``podman run -it -v ./data:/data -p 8001:80 <container-ID>``
* Falls das Data-Directory noch leer ist muss in diesem Schritt das Admin-Passwort eingegeben werden.
* Für Production ``podman run -d -v ./data:/data -p 8001:80 <container-ID>``

Einladungs-Emails
-----------------

Das Script nutzt SMTP mit Verschlüsselung um die E-Mails loszuwerden.
Du wirst hier wahrscheinlich mindestens das Passwort für deinen E-Mail Account
eintragen müssen (und dann *nicht* commiten, hint hint!).

